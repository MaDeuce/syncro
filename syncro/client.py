import logging
import requests


logger = logging.getLogger(__file__)


class SyncroClient:
    """
    Client for Syncro API
    """

    def __init__(self, api_key, api_url):

        self.api_key = api_key
        self.api_url = api_url

        self.headers = {
            "Accept": "application/json",
        }

    def get(self, endpoint):
        """
        Perform HTTP GET on endpoint.

        Args:
            endpoint: str
                A string representing the API endpoint to perform a GET on.
                Example: "/schedules"
        """

        # The params argument to get is added to the "query string".  In this
        # case, it's simply how the api_key is added.
        params = {"api_key": self.api_key}

        if endpoint.startswith("/"):
            # remove leading slash, if present
            endpoint = endpoint[1:]
        url = f"{self.api_url}/{endpoint}"
        response = requests.get(url, headers=self.headers, params=params)

        # This is the simplest way to check for errors.  It will simply raise an
        # exception if response.status_code is 4XX or 5XX.  You will need to do
        # more later, but this gets you started.
        response.raise_for_status()

        # Convert the content of response, which is in JSON, as a python dict.
        return response.json()


if __name__ == "__main__":

    # There are better ways to handle the key, but just to get started,
    # do it the easiest way.  Because the key is equivalent to a password,
    # you never want to check code with a real key into souce code control,
    # such as 'git'.
    API_KEY = "123456789"

    API_SUBDOMAIN = "subdomain"
    API_VERSION = "v1"
    API_URL = f"https://{API_SUBDOMAIN}.syncromsp.com/api/{API_VERSION}"

    client = SyncroClient(api_key=API_KEY, api_url=API_URL)

    invoices = client.get("/invoices")

    # Just a crude example.  Note this will only print the first page of
    # invoices.  Paging will need to be implemented to support pages 2 through
    # 'n'.  Easy to do, once this basic example is working.
    for invoice in invoices:
        print(invoice)
