import logging

from client import SyncroClient


logger = logging.getLogger(__file__)


if __name__ == "__main__":

    # There are better ways to handle the key, but just to get started,
    # do it the easiest way.  Because the key is equivalent to a password,
    # you never want to check code with a real key into souce code control,
    # such as 'git'.
    API_KEY = "123456789"

    API_SUBDOMAIN = "subdomain"
    API_VERSION = "v1"
    API_URL = f"https://{API_SUBDOMAIN}.syncromsp.com/api/{API_VERSION}"

    client = SyncroClient(api_key=API_KEY, api_url=API_URL)

    invoices = client.get("/invoices")

    # Just a crude example.  Note this will only print the first page of
    # invoices.  Paging will need to be implemented to support pages 2 through
    # 'n'.  Easy to do, once this basic example is working.
    for invoice in invoices:
        print(invoice)
